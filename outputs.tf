output "cdn__cloudfront__domain_name" {
  value = aws_cloudfront_distribution.s3_cdn_distribution.domain_name
}

output "cdn__s3_bucket_arn" {
  value = aws_s3_bucket.s3-cdn.arn
}