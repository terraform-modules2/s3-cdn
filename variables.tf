variable "cloudfront__s3_origin_id" {type = string}

variable "cdn__s3__bucket_name" {type = string}
variable "cdn__s3__tags_name" {type = string}
variable "cdn__s3__tags_storage_group_tag" {type = string}

variable "cdn__cloudfront__allowed_methods" {type = list(string)}
variable "cdn__cloudfront__cached_methods" {type = list(string)}
variable "cdn__cloudfront__viewier_protocol_policy" {type = string}
variable "cdn__cloudfront__min_ttl" {type = number}
variable "cdn__cloudfront__default_ttl" {type = number}
variable "cdn__cloudfront__max_ttl" {type = number}
variable "cdn__cloudfront__forward_query_string" {type = bool}
variable "cdn__cloudfront__forward_cookies" {type = string}
variable "cdn__cloudfront__alias_names" {type = list(string)}
variable "cdn__cloudfront__geo_restriction_restriction_type" {type = string}
variable "cdn__cloudfront__ssl_support_method" {type = string}
variable "cdn__cloudfront__logging_include_cookies" {type = bool}
variable "cdn__cloudfront__logging_prefix" {type = string}
variable "cdn__cloudfront__logging_bucket_domain_name" {type = string}
variable "cdn__cloudfront__acm_certificate_arn" {type = string}
variable "cdn__cloudfront__tags_networking_group_tag" {type = string}