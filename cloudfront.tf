resource "aws_cloudfront_distribution" "s3_cdn_distribution" {
  enabled = true
  default_cache_behavior {
    allowed_methods = var.cdn__cloudfront__allowed_methods
    cached_methods = var.cdn__cloudfront__cached_methods
    target_origin_id = var.cloudfront__s3_origin_id
    viewer_protocol_policy = var.cdn__cloudfront__viewier_protocol_policy
    min_ttl                = var.cdn__cloudfront__min_ttl
    default_ttl            = var.cdn__cloudfront__default_ttl
    max_ttl                = var.cdn__cloudfront__max_ttl

    forwarded_values {
      query_string = var.cdn__cloudfront__forward_query_string
      cookies {
        forward = var.cdn__cloudfront__forward_cookies
      }
    }
  }

  origin {
    domain_name = aws_s3_bucket.s3-cdn.bucket_regional_domain_name
    origin_id = var.cloudfront__s3_origin_id
  }

  aliases = var.cdn__cloudfront__alias_names

  restrictions {
    geo_restriction {
      restriction_type = var.cdn__cloudfront__geo_restriction_restriction_type
    }
  }

  viewer_certificate {
    acm_certificate_arn = var.cdn__cloudfront__acm_certificate_arn
    ssl_support_method = var.cdn__cloudfront__ssl_support_method
  }

  logging_config {
    include_cookies = var.cdn__cloudfront__logging_include_cookies
    bucket = var.cdn__cloudfront__logging_bucket_domain_name
    prefix = var.cdn__cloudfront__logging_prefix
  }

  tags = {
    group = var.cdn__cloudfront__tags_networking_group_tag
  }

  depends_on = [
    aws_s3_bucket.s3-cdn
  ]
}