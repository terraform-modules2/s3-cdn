resource "aws_s3_bucket" "s3-cdn" {
  bucket = var.cdn__s3__bucket_name
  tags = {
    Name = var.cdn__s3__tags_name
    group = var.cdn__s3__tags_storage_group_tag
  }
}